const uploadFile = require("../_service/upload");
const unzipper = require("unzipper");
const fs = require("fs");

const upload = async (req, res) => {
    try {
        // -- Upload file --
        await uploadFile(req, res);

        if (req.file == undefined) {
            return res.status(400).send({ message: "No file found. Please upload a file." });
        }

        if (req.file.mimetype === "application/zip") {
            // -- Extract the zip file --
            extractZipFile(req.file.originalname);
        }

        res.status(200).send({
            message: "File is successfully uploaded",
            image: "http://localhost:3000/uploads/" + req.file.originalname,
        });

    } catch (err) {
        res.status(500).send({
            message: `File is not successfully uploaded: ${err}`,
        });
    }
};

const extractZipFile = (fileName) => {
    fs.createReadStream(__basedir + "/uploads/" + fileName)
        .pipe(unzipper.Extract({ path: __basedir + "/uploads/" }));
}
module.exports = {
    upload,
    extractZipFile,
};