const express = require("express");
const controller = require("./_controller/upload");
const app = express();
global.__basedir = __dirname;

app.post("/upload", controller.upload);

app.listen(3000, () => {
    console.log("Server running on port 3000");
});